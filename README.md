# Fiche DIU Bloc 3 
## Convergence IP : la mobilité sur les réseaux WAN (GSM, 2G, 3G, 4G, 5G) 
### Fabrice Missonnier

## Consignes pour accéder à la fiche
Cloner le projet GitLab avec la commande
`git clone https://gitlab.com/missonnier/diu-bloc3-fiche.git`

## Note au lecteur 
Cette fiche étant créée sous Jupyter Lab (comme demandé initialement par Sébastien), il semble nécessaire d'utiliser cet outil pour sa lecture. Les fichiers pdf sont disponibles au cas où, mais on perd vraiment beaucoup d'informations et de mise en page.

## Contenu de ce projet GitLab

Cette fiche présente l'évolution de la téléphonie mobile depuis les années 70. 

Elle s'adresse logiquement à un public de première et de terminale et permet un travail inductif avec de nombreuses questions concrétes sur l'ensemble des notions (voir détails pédagogiques dans la fiche enseignant).

Pour tenter de coller au programme de terminale, cette fiche aborde les bases du réseau téléphonique commuté et son évolution vers un réseau IP. L'objet est de spécifier concrétement les étapes de la convergence vers un réseau unique IP.

Le projet comprend deux fiches : 
 - celle pour l'enseignant (liens relatifs aux programmes de première et de terminale, objectifs pédagigiques, correction des questions)
 - celle pour l'élève (la fiche à produire).
 
